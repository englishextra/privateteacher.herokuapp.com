# privateteacher.herokuapp.com

A Private Teacher of English in Tushino (the homepage)

[![privateteacher.herokuapp.com](http://farm8.staticflickr.com/7331/27601654695_c248f37642_o.jpg)](https://privateteacher.herokuapp.com/)

## On-line

 - [the website](https://privateteacher.herokuapp.com/)
 
## Dashboard

<https://dashboard.heroku.com/apps/privateteacher>
 
## Production Push URL

```
https://git.heroku.com/privateteacher.git
```

## Remotes

 - [GitHub](https://github.com/englishextra/privateteacher.herokuapp.com)
 - [BitBucket](https://bitbucket.org/englishextra/privateteacher.herokuapp.com)
 - [GitLab](https://gitlab.com/englishextra/privateteacher.herokuapp.com)
